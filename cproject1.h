
#include<stdio.h>
#include<string.h>

/***********************************************************************************
 * @file cproject.c struct Books This is used for the declaration of a structure Named Book.
 * This consists of Title, Author, Subject, and Book Id..
 * @file cproject.c This file uses the structure Books. 
 **********************************************************************************/
struct Books {
   char  title[50];  //!< Title can be upto 50 letters 
   char  author[50]; //!< Author name can be upto 50 letters 
   char  subject[100]; //!< Subject name can be upto 100 letters 
   int   book_id; //!< This is an integer so this can take any readable number 
};

/*************************************************************************************************************
  * This is the detailed description for this CProject
  * @file cproject.c This consists of function Bookfunc().
  *
  * int Bookfunc() This function is used to store the title, author, subject, and the book_id in the structure. 
  * This prints the values stored in the structure.
  *
  * @file cproject.c This consists of the structure Books, where the values from Bookfunc() are stored.. 
  * 
  * @return Bookfunc() function returns void.
  ***********************************************************************************************************/
int Bookfunc()
{  //! This is the line-by-line function description for Bookfunc()...
   struct Books Book1;  //! Declare Book1 of type Book   
        
   struct Books Book2; //! Declare Book2 of type Book     
 
  /** This contains Book 1 specification as follows: */ 
   strcpy( Book1.title, "C Programming"); //! ->This line copies C Programming to Title of the structure Book Book1.
   strcpy( Book1.author, "Dennis M. Ritchie"); //! ->This line copies Dennis M. Ritchie to Author of the structure Book Book1.
   strcpy( Book1.subject, "C Programming Tutorial");//! ->This line copies C Programming Tutorial to Subject of the structure Book Book1.
   Book1.book_id = 6495407; //! ->book_id of Book1 is generated in this line.

  /** This contains Book 2 specification as follows: */
   strcpy( Book2.title, "Discrete Mathematics And It's Applications");//! ->This line copies Discrete Mathematics And It's Applications to Title of the structure Book Book2.
   strcpy( Book2.author, "Kenneth H. Rosen");//! ->This line copies Kenneth H. Rosen to Author of the structure Book Book2.
   strcpy( Book2.subject, "Discrete Mathematics Tutorial"); //! ->This line copies Discrete Mathematics Tutorial to Subject of the structure Book Book2.
   Book2.book_id = 6495700; //! ->book_id. of Book2 is generated in this line.
 
  /** This prints Book1 info as the follows: */
   printf( "Book 1 title : %s\n", Book1.title); //! ->Prints Book1 Title.
   printf( "Book 1 author : %s\n", Book1.author); //! ->Prints Book1 Author.
   printf( "Book 1 subject : %s\n", Book1.subject); //! ->Prints Book1 Subject.
   printf( "Book 1 book_id : %d\n", Book1.book_id); //! ->Prints Book1 book_id.

  /** This prints Book2 info as follows: */ 
   printf( "Book 2 title : %s\n", Book2.title); //! ->Prints Book2 Title.
   printf( "Book 2 author : %s\n", Book2.author); //! ->Prints Book2 Author.
   printf( "Book 2 subject : %s\n", Book2.subject); //! ->Prints Book2 Subject.
   printf( "Book 2 book_id : %d\n", Book2.book_id); //! ->Prints Book2 book_id.

   return 0; //! Returns 0 since the function is declared as integer.
}

/***********************************************************************************************
 * int main() This is the main(), where the program starts.
 * @file cproject.c The main() is declared in this file. Without main(), nothing is possible in a C-code.
 * This is used to call the function Bookfunc(), which contains the details of few books.
 * 
 * @return main() returns void.
************************************************************************************************/
  
int main( ) {   //! Declaration of Main Function 
 
   Bookfunc(); //! Calling the function Bookfunc().. 

   return 0;  //! Returns 0 since main() is declared as an integer. 
}
